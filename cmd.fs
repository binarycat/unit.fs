1000 constant _len
_len buffer: _buf
: quick-conv
  next-arg S" amount" replaces
  next-arg S" from" replaces
  next-arg S" into" replaces
  S" fconv %amount%e %from% fas %into% f. cr"
  _buf _len substitute 0 min throw evaluate ;

: run-cmd
  argc @ 4 = if quick-conv bye then ;

:noname defers 'cold run-cmd ; is 'cold
  
require safe.fs
