\ core functionality

' * value _as
0 value _as1

: as ['] / to _as ;
: (unit) _as to _as1 ['] * to _as ;
: unit: create , does>
  @ _as execute
  (unit) ;

: units: ( mul initial count "<names>" -- )
  0 ?do \ mul current
    dup unit: over *
  loop 2drop ;

[DEFINED] f/ [IF]
  \ gforth mixed arithmetic
  [UNDEFINED] fm/ [IF]
    : fm/ s>f f/ ;
    : fm* s>f f* ;
  [THEN]
  [UNDEFINED] ftuck [IF]
    : ftuck fdup frot frot ;
  [THEN]
  : fconv ['] fm* to _as ;
  : fas ['] fm/ to _as ;
[THEN]

\ compound units

: as-both ( u "<name>" -- asunit rem )
  ' 2dup as execute \ u unitxt asunit
  dup >r swap execute -
  r> swap as ;

[DEFINED] fas [IF]
  \ assuming seperate float and paramater stacks
  : fas-both
    ' dup fdup fas execute \ unitxt F: r r.asunit
    floor ftuck fconv execute f- fas ;
[THEN]

: [square] _as 2>r
  2r@ to _as execute
  2r> to _as execute ;

: square
  ' [square] ;

: [cubic] _as 2>r
  2r@ to _as execute
  2r@ to _as execute
  2r> to _as execute ;

: cubic ' [cubic] ;

: per _as1 case
    ['] * of ['] / endof
    ['] / of ['] * endof
    [DEFINED] f/ [IF]
      ['] fm* of ['] fm/ endof
      ['] fm/ of ['] fm* endof
    [THEN]
    abort
  endcase to _as ;
