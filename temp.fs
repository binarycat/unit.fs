require core.fs

\ we need symmetric division to round temperatures consistantly
-10 7 / -1 = [IF]
  : symmetric/ / ;
[ELSE]
  : symmetric/ >R S>D R> SM/REM SWAP DROP ;
[THEN]

\ internal representation of temperature is centikelvins
100 unit: kelvin

: celsius
  _as case
    ['] * of 100 * 27315 + endof \ celsius to centikelvins
    ['] / of 27315 - 100 symmetric/ endof \ centikelvins to celsius
    [DEFINED] f/ [IF]
      ['] fm* of 100e f* 27315e f+ endof
      ['] fm/ of 27315e f- 100e f/ endof
    [THEN]
    [DEFINED] .word [IF] dup .word [THEN]
    true abort" invalid value of _as"
  endcase (unit) ; 

: fahrenheit
  _as case
    ['] * of 100 * 45967 + 5 * 9 symmetric/ endof
    ['] / of 9 * 5 symmetric/ 45967 - 100 symmetric/ endof
    [DEFINED] f/ [IF]
      ['] fm* of 100e f* 45967e f+ 5e f* 9e f/ endof
      ['] fm/ of 9e f* 5e f/ 45967e f- 100e f/ endof
    [THEN]
    abort
  endcase (unit) ;
