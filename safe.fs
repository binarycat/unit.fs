require compat.fs
\ finalize the interpreter as something that can
\ only be used for unit conversion and basic math

\ this is gforth-only until pforth adds support for wordlists

: keep[
  begin
    >in @ '
    dup ['] ] <> while
    swap >in ! alias
  repeat 2drop ;
vocabulary unit-voc
unit-voc definitions
require all.fs


keep[ + - * / . ]
[DEFINED] f/ [IF] keep[ f+ f- f* f/ f. ] [THEN]

keep[ cr words bye ]

keep[ savesystem ]

unit-voc seal

