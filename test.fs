require unit_defs.fs
require temp.fs
require test/ttester.fs


T{ 360 degrees -> 400 gradient }T
T{ 1 megabytes as megabits -> 8 }T
T{ 3 megabytes -> 3000000 bytes }T
T{ 2 sectors -> 1 kibibytes }T

T{ 5 acres as square feet -> 217800 }T

T{ 1 light-seconds -> 299792458 meters }T
T{ 1 light-years -> 31557600 light-seconds }T


testing temperature
T{ 0 kelvin as celsius -> -273 }T
T{ -273 celsius as kelvin -> 0 }T
T{ -459 fahrenheit as kelvin -> 0 }T
T{ 100 celsius as celsius -> 100 }T
T{ 0 celsius as fahrenheit -> 32 }T

testing floating point
T{ fconv 12e inches -> fconv 1e feet }T
T{ fconv 1e quarts fas gallons -> 0.25e }T

testing floating point temperature
T{ fconv 0e kelvin fas fahrenheit -> -459.67e }T
T{ fconv 0e kelvin fas celsius -> -273.15e }T

testing compound units
\ when using as-both, units must always be listed largest to smallest
T{ 100 seconds as-both minutes seconds -> 1 40 }T
T{ 3700 seconds as-both hours as-both minutes seconds -> 1 1 40 }T
T{ 6 feet 3 inches + as inches -> 75 }T
bye

