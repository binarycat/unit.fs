# unit conversions in forth

## usage

users who are not familiar with FORTH are recommeded to use:

    $ gforth safe.fs

as `safe.fs` removes access to all words that could cause permnant changes to your system, such as deleting files.

if you are an experienced FORTH user and need access to more than just basic arithmetic:

    $ gforth all.fs

## examples

    10 days as hours .
    fconv 1e cups fas gallons f.
    6 feet 3 inches + as centimeters .
    75 inches as-both feet inches .
    fconv 10e miles per hours fas feet per seconds .s

