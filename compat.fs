[UNDEFINED] alias [IF] : alias create , does> @ execute ; [THEN]

\ make gforth able to include relative to current directory instead of
\ relative to location of source file.
\ this allows using a nested directory structure while
\ still maintaing compatibility with different forths.
[DEFINED] fpath [IF] pad 100 get-dir fpath also-path [THEN]
