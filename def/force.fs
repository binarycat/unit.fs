require def/mass.fs
require def/distance.fs
require def/time.fs

1 kilogram meter per second per second unit: newtons
1000 newtons unit: kilonewtons

a: newtons newton N
a: kilonewtons kilonewton kN

1 pound foot per second per second unit: poundals

a: poundals poundal pdl

