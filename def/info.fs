\ units of information

1 unit: bits
8 bits unit: bytes
512 bytes unit: sectors

1000 dup bits
6 units: kilobits megabits gigabits terabits petabits exabits

1000 dup bytes
6 units: kilobytes megabytes gigabytes terabytes petabytes exabytes

1024 bytes unit: kibibytes
1024 kibibytes unit: mebibytes
1024 mebibytes unit: gibibytes
1024 gibibytes unit: tebibytes

a: bits bit
a: kilobits kilobit kbit
a: megabits megabit Mbit
a: gigabits gigabit Gbit

a: bytes byte B
a: kilobytes kilobyte kB
a: megabytes megabyte MB
a: gigabytes gigabyte GB
a: terabytes terabyte TB

a: kibibytes kibibyte KiB
a: mebibytes mebibyte MiB
a: gibibytes gibibyte GiB
a: tebibytes tebibyte TiB

: KiB/s KiB per second ;
: MiB/s MiB per second ;
: GiB/s GiB per second ;
: TiB/s TiB per second ;
