1 unit: micrograms
1000 micrograms unit: milligrams
1000 milligrams unit: grams
1000 grams unit: kilograms

\ TODO: newtons?


a: milligrams milligram mg
a: grams gram g
a: kilograms kilogram kg

\ "pound" here refers to pound-mass
453592370 micrograms unit: pounds
2000 pounds unit: tons

a: pounds pound lbs lb
a: tons ton
