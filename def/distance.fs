   1             unit: micrometers
1000 micrometers unit: millimeters
  10 millimeters unit: centimeters
  10 centimeters unit: decimeters
 100 centimeters unit: meters
1000 meters      unit: kilometers

25400 micrometers unit: inches
   12 inches      unit: feet
    4 inches      unit: hands
    3 feet        unit: yards
 5280 feet        unit: miles

149597870700 meters        unit: astronomical-units
   299792458 meters        unit: light-seconds
          60 light-seconds unit: light-minutes
          60 light-minutes unit: light-hours
          24 light-hours   unit: light-days
           7 light-days    unit: light-weeks
          30 light-days    unit: light-months
    31557600 light-seconds unit: light-years \ 365.25 light days
\ parsecs would be the next step up, but they are imposible to represent due to
\ limited integer precicion

a: micrometers micrometer
a: millimeters millimeter mm
a: centimeters centimeter cm
a: decimeters decimeter dm
a: meters meter m
a: kilometers kilometer km

a: feet foot ft
a: yards yard yd
a: inches inch in
a: miles mile mi

a: astronomical-units astronomical-unit au
a: light-seconds light-second
a: light-minutes light-minute
a: light-hours light-hour
a: light-days light-day
a: light-weeks light-week
a: light-months light-month
a: light-years light-year ly
