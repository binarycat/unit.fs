require def/distance.fs

\ # volume #
1 cubic centimeters unit: milliliters
1000 milliliters unit: liters
1000 liters unit: kiloliter

\ using US legal definitions instead of US customary becasue the definitions are much cleaner
  5 milliliters unit: teaspoons
 15 milliliters unit: tablespoons
240 milliliters unit: cups
  2 cups        unit: pints
  2 pints       unit: quarts
  4 quarts      unit: gallons

a: teaspoons teaspoon tsp
a: tablespoons tablespoon tbsp
a: cups cup
a: pints pint
a: quarts quart
a: gallons gallon gal


\ toss in area units with volume, there aren't enough to warrent making them seperate
4840 square yards unit: acres
10000 square meters unit: hectares
