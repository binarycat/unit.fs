\ misc unit definitions

\ electrical

1 unit: millivolts
1000 millivolts unit: volts
1000 volts unit: kilovolts

\ TODO: define watts properly so multiplying volts and amps gives you the correct amount of watts after conversion

\ # frequency #

1 unit: hertz
1000 hertz unit: kilohertz
1000 kilohertz unit: megahertz
1000 megahertz unit: gigahertz

\ TODO: units for amounts of paper (eg. ream)

\ # angle units #
1 unit: arc-seconds
60 arc-seconds unit: arc-minutes
60 arc-minutes unit: degrees

3240 arc-seconds unit: gradient

\ radians??

\ # scalar values #

20 unit: score
12 unit: dozen
1 dozen dozen unit: gross

\ TODO: moles

a: hertz Hz
a: kilohertz kHz
a: megahertz MHz
a: gigahertz GHz


\ complex units
: mph miles per hour ;
: m/s meters per second ;
: sqft ' feet [square] ;
